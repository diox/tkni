﻿namespace tkni_common.Dto
{
    public class PlayerInfo
    {
        public string ID { get; set; }

        public int X { get; set; }

        public int Y { get; set; }

        public int? StamenDirection { get; set; }

        public int AttackersCount { get; set; }

        public bool IsDead { get; set; }

        public string Name { get; set; }
    }
}
