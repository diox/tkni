﻿using System.Collections.Generic;

namespace tkni_common.Dto
{
    public class SceneState
    {
        public int BackgroundX { get; set; }

        public int BackgroundY { get; set; }

        public int? HitDirection { get; set; }

        public List<PlayerInfo> Others { get; set; }

        public bool IsMoving { get; set; }

        public string Name { get; set; }

        public bool IsKilled { get; set; }

        public int AttackersCount { get; set; }
    }
}
