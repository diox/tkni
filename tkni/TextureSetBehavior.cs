﻿namespace tkni
{
    public enum TextureSetBehavior
    {
        Repeat,
        OneTime,
        Blink
    }
}
