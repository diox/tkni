﻿using Microsoft.Xna.Framework.Graphics;

namespace tkni.GameObjects
{
    public class Background
    {
        private readonly Texture2D _texture;

        public Background()
        {
            _texture = ContentManager.Textures.Background;
            X = 30;
            Y = 45;
        }

        public int X { get; set; }

        public int Y { get; set; }

        public int Width => _texture.Width;

        public int Height => _texture.Height;
    }
}
