﻿using Microsoft.Xna.Framework;

using tkni_common;

namespace tkni.GameObjects
{
    public abstract class Vacuole : StateObject<VakaStates>
    {
        protected Vacuole()
            : base(VakaStates.Idle, (state1, state2) => state1 == state2, new Vector2(400, 400), 0.1f)
        {
            AddTextureSet(VakaStates.Idle, new TextureSet(Constants.ClientAnimationTimeToFrame, ContentManager.Textures.VakaStand));
            AddTextureSet(VakaStates.Move, new TextureSet(Constants.ClientAnimationTimeToFrame, ContentManager.Textures.VakaGo));
            AddTextureSet(VakaStates.Hit, new TextureSet(Constants.ClientAnimationTimeToFrame, TextureSetBehavior.Blink, ContentManager.Textures.VakaBeat));
            AddTextureSet(VakaStates.Attacked, new TextureSet(Constants.ClientAnimationTimeToFrame, ContentManager.Textures.FoeStand));
            AddTextureSet(VakaStates.DoubleAttacked, new TextureSet(Constants.ClientAnimationTimeToFrame, ContentManager.Textures.FoeDance));
            AddTextureSet(VakaStates.Destroyed, new TextureSet(Constants.ClientAnimationTimeToFrame, TextureSetBehavior.OneTime, new Vector2(1000, 1000), ContentManager.Textures.FoeExplode));
        }

        public string Name { get; set; }

        public static VakaStates GetState(int attackersCount, bool isDead)
        {
            if (isDead)
            {
                return VakaStates.Destroyed;
            }

            switch (attackersCount)
            {
                case 0:
                    return VakaStates.Idle;

                case 1:
                    return VakaStates.Attacked;

                case 2:
                    return VakaStates.DoubleAttacked;

                default:
                    return VakaStates.Destroyed;
            }
        }
    }
}
