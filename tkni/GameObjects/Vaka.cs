﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using tkni_common;

namespace tkni.GameObjects
{
    public class Vaka : Vacuole
    {
        public Vaka()
        {
            Position = new Vector2(Constants.ClientWindowWidth, Constants.ClientWindowHeight) / 2;
            TintColor = Color.Beige;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            spriteBatch.DrawString(ContentManager.Fonts.Main, Name ?? string.Empty, Position - new Vector2(50, 60), Color.MonoGameOrange);
        }

        public bool CanActing()
        {
            return CurrentState == VakaStates.Idle || CurrentState == VakaStates.Move || CurrentState == VakaStates.Hit;
        }
    }
}
