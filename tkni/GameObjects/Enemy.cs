﻿using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace tkni.GameObjects
{
    public class Enemy : Vacuole
    {
        public string Id { get; set; }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            spriteBatch.DrawString(ContentManager.Fonts.Main, Name, Position - new Vector2(50, 20), Color.Purple);
        }

        public class Comparer : IEqualityComparer<Enemy>
        {
            public bool Equals(Enemy x, Enemy y)
            {
                return x.Id == y.Id;
            }

            public int GetHashCode(Enemy obj)
            {
                return obj.Id.GetHashCode();
            }

            public static Comparer Instance { get; } = new Comparer();
        }

        public static ICollection<Enemy> SynchronizeLists(ICollection<Enemy> firstList, ICollection<Enemy> secondList)
        {
            return SynchronizeListsIterator().ToList();

            IEnumerable<Enemy> SynchronizeListsIterator()
            {
                foreach (Enemy e1 in firstList)
                {
                    Enemy e2 = secondList.FirstOrDefault(e => e.Id == e1.Id);
                    if (e2 != null)
                    {
                        e1.Position = e2.Position;
                        e1.CurrentState = e2.CurrentState;
                        yield return e1;
                    }
                }

                foreach (Enemy e2 in secondList.Except(firstList, Enemy.Comparer.Instance))
                {
                    yield return e2;
                }
            }
        }
    }
}
