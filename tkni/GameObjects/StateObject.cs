﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace tkni.GameObjects
{
    public abstract class StateObject<TState>
    {
        private TState _currentState;
        private readonly Func<TState, TState, bool> _stateComparer;
        private readonly Dictionary<TState, TextureSet> _textures;
        private readonly Vector2 _origin;
        private readonly float _scale;

        protected StateObject(TState initialState, Func<TState, TState, bool> stateComparer, Vector2 origin, float scale)
        {
            _currentState = initialState;
            _stateComparer = stateComparer;
            _textures = new Dictionary<TState, TextureSet>();
            _origin = origin;
            _scale = scale;
        }

        public Texture2D CurrentTexture => _currentTextureSet.CurrentTexture;

        public Vector2 Position { get; set; }

        public TState CurrentState
        {
            get => _currentState;
            set
            {
                if (!_stateComparer(_currentState, value))
                {
                    _currentTextureSet.Reset();
                    _currentState = value;
                }
            }
        }

        public float Rotation { get; set; }

        public void Update(GameTime gameTime)
        {
            _currentTextureSet.Elapse(gameTime.ElapsedGameTime);
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(CurrentTexture, Position, null, TintColor, Rotation, _currentTextureSet.Origin ?? _origin, _scale, SpriteEffects.None, 0f);
        }

        protected void AddTextureSet(TState state, TextureSet textureSet)
        {
            _textures[state] = textureSet;
        }

        protected Color TintColor { get; set; } = Color.White;

        private TextureSet _currentTextureSet => _textures[_currentState];
    }
}
