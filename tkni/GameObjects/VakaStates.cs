﻿namespace tkni.GameObjects
{
    public enum VakaStates
    {
        Idle,
        Move,
        Hit,
        Attacked,
        DoubleAttacked,
        Destroyed,
    }
}
