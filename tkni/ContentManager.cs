﻿using System.Linq;
using Microsoft.Xna.Framework.Graphics;

namespace tkni
{
    public static class ContentManager
    {
        public static Microsoft.Xna.Framework.Content.ContentManager Content { get; set; }
        
        public static class Textures
        {
            private const string prefix = "textures/";

            public static Texture2D Background { get; } = Content.Load<Texture2D>(prefix + "background");

            public static Texture2D[] VakaStand { get; } = Enumerable.Range(1, 5)
                .Select(i => Content.Load<Texture2D>(prefix + "vakuolka/vakuolka_stand_" + i))
                .ToArray();

            public static Texture2D[] VakaGo { get; } = Enumerable.Range(1, 6)
                .Select(i => Content.Load<Texture2D>(prefix + "vakuolka/vakuolka_go_" + i))
                .ToArray();

            public static Texture2D[] VakaBeat { get; } = Enumerable.Range(1, 8)
                .Select(i => Content.Load<Texture2D>(prefix + "vakuolka/vakuolka_bit_" + i))
                .ToArray();

            public static Texture2D[] FoeStand { get; } = Enumerable.Range(1, 5)
                .Select(i => Content.Load<Texture2D>(prefix + "vakuolka/foe_stand_" + i))
                .ToArray();

            public static Texture2D[] FoeDance { get; } = Enumerable.Range(1, 9)
                .Select(i => Content.Load<Texture2D>(prefix + "vakuolka/foe_dance_" + i))
                .ToArray();

            public static Texture2D[] FoeExplode { get; } = Enumerable.Range(1, 9)
                .Select(i => Content.Load<Texture2D>(prefix + "vakuolka/foe_explode_" + i))
                .ToArray();
        }

        public static class Fonts
        {
            private const string prefix = "fonts/";

            public static SpriteFont Main { get; } = Content.Load<SpriteFont>(prefix + "Main");
        }
    }
}
