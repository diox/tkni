﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.SignalR.Client;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using tkni.GameObjects;
using tkni_common;
using tkni_common.Dto;

namespace tkni
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class TkniGame : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        private HubConnection _connection;
        private IHubProxy _stateHubProxy;
        private int _targetDirection;

        private SceneState _currentState;
        private Background _background;
        private Vaka _vaka;
        private ICollection<Enemy> _enemies;

        private int _attackersCount;

        private TimeSpan _hitElapsed;

        public TkniGame()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferWidth = Constants.ClientWindowWidth;
            graphics.PreferredBackBufferHeight = Constants.ClientWindowHeight;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            Window.Title = "STICK THE VACUOLE // #ottepelhack // 2018";
            IsMouseVisible = true;

            _connection = new HubConnection(Constants.ServerAddress);

            _stateHubProxy = _connection.CreateHubProxy(Constants.Hubs.State.Name);
            _stateHubProxy.On(Constants.Hubs.State.Methods.Update, new Action<SceneState>(ProcessUpdateState));

            base.Initialize();
        }

        /// <summary> 
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            ContentManager.Content = Content;

            _background = new Background();
            _vaka = new Vaka();
            _enemies = new HashSet<Enemy>();

            _connection.Start().GetAwaiter().GetResult();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            KeyboardState keyboardState = Keyboard.GetState();
            MouseState mouseState = Mouse.GetState();

            if (keyboardState.IsKeyDown(Keys.Escape))
            {
                Exit();
            }

            if (_vaka.CanActing())
            {
                int? direction = GetDirection(keyboardState);
                if (direction.HasValue)
                {
                    _targetDirection = direction.Value;
                    _stateHubProxy.Invoke(Constants.Hubs.State.Methods.Move, _targetDirection);
                }

                _hitElapsed += gameTime.ElapsedGameTime;
                
                if (mouseState.LeftButton == ButtonState.Pressed && _hitElapsed > TimeSpan.FromMilliseconds(400))
                {
                    _hitElapsed = TimeSpan.Zero;
                    Vector2 hitVector = new Vector2(mouseState.X, mouseState.Y) - _vaka.Position;
                    int hitDirection = (int)MathHelper.ToDegrees((float)Math.Atan2(hitVector.Y, hitVector.X));
                    _stateHubProxy.Invoke(Constants.Hubs.State.Methods.Hit, hitDirection);
                }

                if (_vaka.CurrentState == VakaStates.Idle ||
                    _vaka.CurrentState == VakaStates.Move)
                {
                    _vaka.Rotation = MathHelper.ToRadians(_targetDirection);
                }
            }

            _vaka.Update(gameTime);

            foreach (Enemy enemy in _enemies.ToList())
            {
                enemy.Update(gameTime);
            }
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            Color painTint = GetPainTint();

            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();

            for (int x = _background.X - _background.Width; x < Constants.ClientWindowWidth; x += _background.Width)
            {
                for (int y = _background.Y - _background.Height; y < Constants.ClientWindowHeight; y += _background.Height)
                {
                    spriteBatch.Draw(ContentManager.Textures.Background, new Vector2(x, y), painTint);
                }
            }

            foreach (var enemy in _enemies) 
            {
                enemy.Draw(spriteBatch);
            }

            _vaka.Draw(spriteBatch);

            spriteBatch.End();

            base.Draw(gameTime);
        }

        private static int? GetDirection(KeyboardState keyboardState)
        {
            bool right = keyboardState.IsKeyDown(Keys.Right);
            bool left = keyboardState.IsKeyDown(Keys.Left);
            bool up = keyboardState.IsKeyDown(Keys.Up);
            bool down = keyboardState.IsKeyDown(Keys.Down);

            if (left && right)
            {
                left = false;
                right = false;
            }

            if (up && down)
            {
                up = false;
                down = false;
            }

            if (down)
            {
                if (left) return 135;
                if (right) return 45;
                return 90;
            }

            if (up)
            {
                if (left) return 225;
                if (right) return 315;
                return 270;
            }

            if (left) return 180;
            if (right) return 0;

            return null;
        }

        private void ProcessUpdateState(SceneState state)
        {
            _currentState = state;

            if (state.IsKilled)
            {
                _vaka.CurrentState = VakaStates.Destroyed;
                _attackersCount = Constants.AttacksToKill;
            }

            _background.X = state.BackgroundX;
            _background.Y = state.BackgroundY;
            _vaka.Name = state.Name;

            if (state.HitDirection.HasValue)
            {
                _vaka.CurrentState = VakaStates.Hit;
                _vaka.Rotation = MathHelper.ToRadians(state.HitDirection.Value);
            }
            else
            {
                if (state.AttackersCount > 0)
                {
                    _vaka.CurrentState = Enemy.GetState(state.AttackersCount, state.IsKilled);
                }
                else
                {
                    _vaka.CurrentState = state.IsMoving ? VakaStates.Move : VakaStates.Idle;
                }
            }

            _attackersCount = state.AttackersCount;

            var enemies = state.Others
                .Select(other => new Enemy
                {
                    Id = other.ID,
                    Name = other.Name,
                    Position = new Vector2(other.X, other.Y),
                    CurrentState = Enemy.GetState(other.AttackersCount, other.IsDead),
                })
                .ToList();

            _enemies = Enemy.SynchronizeLists(_enemies, enemies);
        }

        private Color[] painTints = { Color.White, new Color(255, 200, 200), new Color(255, 150, 150), new Color(255, 100, 100) };

        private Color GetPainTint()
        {
            return painTints[_attackersCount < painTints.Length ? _attackersCount : painTints.Length - 1];
        }
    }
}
