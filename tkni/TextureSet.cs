﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace tkni
{
    public class TextureSet
    {
        private readonly TimeSpan _timeToFrame;
        private readonly TextureSetBehavior _behavior;
        private readonly Texture2D[] _textures;
        private int _currentTextureIndex;
        private TimeSpan _elapsed;

        public event Action AnimationComplete;

        public TextureSet(TimeSpan timeToFrame, params Texture2D[] textures)
            :this(timeToFrame, TextureSetBehavior.Repeat, textures)
        {
        }

        public TextureSet(TimeSpan timeToFrame, TextureSetBehavior behavior, params Texture2D[] textures)
            :this(timeToFrame, behavior, null, textures)
        {
        }

        public TextureSet(TimeSpan timeToFrame, TextureSetBehavior behavior, Vector2? origin, params Texture2D[] textures)
        {
            _timeToFrame = timeToFrame;
            _behavior = behavior;
            Origin = origin;
            _textures = textures;
        }

        public Vector2? Origin { get; }

        public void NextTexture()
        {
            int newIndex = _currentTextureIndex + 1;
            if (newIndex >= _textures.Length)
            {
                switch (_behavior)
                {
                    case TextureSetBehavior.Repeat:
                        newIndex = 0;
                        break;

                    case TextureSetBehavior.OneTime:
                        AnimationComplete?.Invoke();
                        return;

                    case TextureSetBehavior.Blink:
                        newIndex = _textures.Length - 2;
                        break;

                    default:
                        throw new InvalidOperationException("Unknown behavior: " + _behavior);
                }
            }

            _currentTextureIndex = newIndex;
        }

        public Texture2D CurrentTexture => _textures[_currentTextureIndex];

        public void Reset()
        {
            _currentTextureIndex = 0;
        }

        public void Elapse(TimeSpan timeSpan)
        {
            _elapsed += timeSpan;
            while(_elapsed > _timeToFrame)
            {
                NextTexture();
                _elapsed -= _timeToFrame;
            }
        }
    }
}
