﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Microsoft.AspNet.SignalR;
using Microsoft.Owin;

using Owin;

using tkni_common;
using tkni_common.Dto;

using tkni_server.Logic;

[assembly: OwinStartup(typeof(tkni_server.Startup))]

namespace tkni_server
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();

            Task.Run(async () =>
            {
                while (true)
                {
                    StateLogic.Single.ProcessGameState();

                    await Task.Delay(Constants.ServerUpdateFrequency);
                }
            });

            Task.Run(async () =>
            {
                IHubContext stateHubContext = GlobalHost.ConnectionManager.GetHubContext<State>();

                while (true)
                {
                    if (StateLogic.Single.IsGameSessionExists)
                    {
                        IEnumerable<string> clientsIDs = StateLogic.Single.GetRegisteredPlayersIDs();

                        foreach (string clientID in clientsIDs)
                        {
                            SceneState clientState = StateLogic.Single.GetPlayerSceneState(clientID);

                            stateHubContext.Clients.Clients(new[] { clientID }).update(clientState);
                        }
                    }

                    await Task.Delay(Constants.ServerUpdateFrequency);
                }
            });
        }
    }
}