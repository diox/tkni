﻿using System;
using System.Numerics;
using tkni_common;

namespace tkni_server.Entities
{
    public class Player
    {
        public string ID { get; set; }

        public string Name { get; set; }

        public Vector2 Position { get; set; }

        public int? DirectionBase { get; set; }

        public int DirectionChange { get; set; }

        public DateTime LastDirectionChangeEventDate { get; set; }

        public int AttackersCount { get; set; }

        public int? StamenDirection { get; set; }

        public bool IsDead { get; set; }

        public DateTime IsDeadEventDate { get; set; }

        public DateTime StamenDirectionChangeEventDate { get; set; }

        public bool IsParalized => IsDead || AttackersCount > 0;

        public bool IsStamenActive => StamenDirection.HasValue && !IsParalized;

        public TimeSpan StamenDuration => DateTime.Now - StamenDirectionChangeEventDate;

        public Vector2 StamenVector => Position + Helpers.GetAngleVector(StamenDirection.Value) * Constants.StamenLength;

        public TimeSpan LastDirectionDuration => DateTime.Now - LastDirectionChangeEventDate;

        public TimeSpan DeadDuration => DateTime.Now - IsDeadEventDate;
    }
}