﻿using System.Collections.Generic;

namespace tkni_server.Entities
{
    public class GameSession
    {
        public List<Player> Players { get; } = new List<Player>();
    }
}