﻿using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using tkni_server.Logic;

namespace tkni_server
{
    public class State : Hub
    {
        public void Move(int direction)
        {
            // send move to logic
            StateLogic.Single.AssignPlayerDirectionChange(this.Context.ConnectionId, direction);
        }

        public void Hit(int direction)
        {
            // send hit to logic
            StateLogic.Single.ActivatePlayerStamen(this.Context.ConnectionId, direction);
        }

        public async override Task OnConnected()
        {
            await base.OnConnected();

            if (!StateLogic.Single.IsGameSessionExists)
            {
                StateLogic.Single.CreateGameSession(this.Context.ConnectionId);
            }
            else
            {
                StateLogic.Single.RegisterNewPlayerToGameSession(this.Context.ConnectionId);
            }
        }
    }
}