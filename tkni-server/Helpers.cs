﻿using System;
using System.Numerics;

using tkni_common;

namespace tkni_server
{
    internal static class Helpers
    {
        public static Vector2 GetAngleVector(int angleInDegrees)
        {
            return GetAngleVector(GetRadiansFromDegrees(angleInDegrees));
        }

        public static Vector2 GetAngleVector(double angleInRadians)
        {
            return new Vector2((float)Math.Cos(angleInRadians), (float)Math.Sin(angleInRadians));
        }

        private static double GetRadiansFromDegrees(int angle) => angle * Math.PI / 180;

        private static double GetDegreesFromRadians(int angle) => angle * 180 / Math.PI;
    }
}