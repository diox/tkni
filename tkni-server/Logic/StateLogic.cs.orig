﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

using tkni_common;
using tkni_common.Dto;
using tkni_server.Entities;
using tkni_server.Enum;

namespace tkni_server.Logic
{
    public class StateLogic
    {
        private GameSession _gameSession;

        public static StateLogic Single { get; } = new StateLogic();

        private StateLogic() { }

        public void ProcessGameState()
        {
            if (_gameSession == null)
            {
                return;
            }

            ProcessAttacks();
            ChangePlayersDirections(_gameSession);
            ChangePlayersPosition(_gameSession);
        }

        public void CreateGameSession(string playerID)
        {
            _gameSession = new GameSession();

            var newPlayer = new Player()
            {
                ID = playerID,
                Position = GetNewPlayerPosition()
            };

            var botPlayer = new Player()
            {
                ID = "guidID",
                Position = GetNewPlayerPosition() + new Vector2(50, 50),
                DirectionBase = 180,
                DirectionChange = 180
            };

            _gameSession.Players.Add(newPlayer);
            _gameSession.Players.Add(botPlayer);
        }

        public void RegisterNewPlayerToGameSession(string playerID)
        {
            var newPlayer = new Player()
            {
                ID = playerID,
                Position = GetNewPlayerPosition()
            };

            _gameSession.Players.Add(newPlayer);
        }

        public bool IsGameSessionExists()
        {
            return _gameSession != null;
        }

        public void ProcessAttacks()
        {
            var attackingPlayers = _gameSession.Players.Where(p => p.IsStamenActive);

            foreach (var player in attackingPlayers)
            {
                var otherPlayers = _gameSession.Players.Where(p => p.ID != player.ID);

                var stamenVector = GetStamenVector(player.Position, player.StamenDirection.Value);

                foreach (var otherPlayer in otherPlayers)
                {
                    if (IsVectorIntersectsCircle(otherPlayer.Position, Constants.PlayerBodyRadius, player.Position, stamenVector))
                    {
                        AddAttackOnPlayer(otherPlayer.ID);
                    }
                }
            }
        }

        public void AddAttackOnPlayer(string playerID)
        {
            Player currentPlayer = _gameSession.Players.FirstOrDefault(x => x.ID == playerID);

            currentPlayer.AttackersCount += 1;
        }

        public IEnumerable<string> GetRegisteredPlayersIDs()
        {
            return _gameSession?.Players.Select(p => p.ID);
        }

        public void AssignPlayerDirectionChange(string playerID, int directionChange)
        {
            Player currentPlayer = _gameSession.Players.FirstOrDefault(x => x.ID == playerID);

            if (currentPlayer != null)
            {
                currentPlayer.DirectionChange = directionChange;
                currentPlayer.LastDirectionChangeEventDate = DateTime.Now;
            }
        }

        public void ActivatePlayerStamen(string playerID, int stamenDirection)
        {
            Player currentPlayer = _gameSession.Players.FirstOrDefault(x => x.ID == playerID);

            if (currentPlayer == null)
                return;

            currentPlayer.StamenDirection = stamenDirection;
        }

        public SceneState GetPlayerSceneState(string playerID)
        {
            Player currentPlayer = _gameSession.Players.FirstOrDefault(x => x.ID == playerID);

            if (currentPlayer == null)
            {
                return null;
            }

            Vector2 backCoor = ConvertPlayerCoorsToBackCoors(currentPlayer.Position);

            return new SceneState
            {
                BackgroundX = (int)backCoor.X,
                BackgroundY = (int)backCoor.Y,
                HitDirection = currentPlayer.StamenDirection,
                Others = GetVisiblePlayersInfo(currentPlayer).ToList()
            };
        }
        
        public IEnumerable<PlayerInfo> GetVisiblePlayersInfo(Player currentPlayer)
        {
            var visiblePlayers = _gameSession.Players.Where(p => p.ID != currentPlayer.ID && IsPlayerVisible(currentPlayer, p));

            return visiblePlayers?.Select(x => new PlayerInfo() { X = (int)x.Position.X, Y = (int)x.Position.Y, AttackersCount = x.AttackersCount, StamenDirection = x.StamenDirection });
        }

        private Vector2 CountUIVector(Vector2 currentUserVector, Vector2 otherUserVector)
        {
            var baseUIVector = new Vector2(-Constants.ClientWindowWidth / 2, -Constants.ClientWindowHeight / 2);

            return -(currentUserVector + baseUIVector - otherUserVector);
        }

        public bool IsPlayerVisible(Player currentPlayer, Player otherPlayer)
        {
            var resVec = otherPlayer.Position - currentPlayer.Position;

            return Math.Abs(resVec.X) < Constants.ClientWindowWidth / 2 && Math.Abs(resVec.Y) < Constants.ClientWindowHeight / 2;
        }

        public Vector2 GetStamenVector(Vector2 playerVector, int stamenDirection)
        {
            var directionInRadians = GetRadiansFromDegrees(stamenDirection);

            var angleSin = Math.Sin(directionInRadians);
            var angleCos = Math.Cos(directionInRadians);

            var delta = new Vector2((float)(Constants.baseLineSpeed * angleCos), (float)(Constants.baseLineSpeed * angleSin));

            return playerVector + delta;
        }

        private bool IsVectorIntersectsCircle(Vector2 circleCent, float radius, Vector2 vectorStart, Vector2 vectorEnd)
        {
            Vector2 vector = vectorEnd - vectorStart;

            float a = vector.LengthSquared();
            if (a <= 0)
            {
                return false;
            }

            Vector2 relativeStart = vectorStart - circleCent;

            float b = 2 * (vector.X * relativeStart.X + vector.Y * relativeStart.Y);
            float c = relativeStart.LengthSquared() - radius * radius;

            float det = b * b - 4 * a * c;

            return det >= 0;
        }

        private Vector2 ConvertPlayerCoorsToBackCoors(Vector2 coor) => -coor;

        private Vector2 GetNewPlayerPosition() => new Vector2(Constants.ClientWindowWidth, Constants.ClientWindowHeight) / 2;

        private void ChangePlayersDirections(GameSession gameSession)
        {
            foreach (Player player in gameSession.Players)
            {
                ChangePlayerDirection(player);
            }
        }

        private void ChangePlayerDirection(Player player)
        {
            if (player.DirectionBase == player.DirectionChange)
            {
                return;
            }

            int directionDelta = (player.DirectionBase - player.DirectionChange + 360) % 360;

            if (directionDelta > 180)
            {
                ModifyPlayerDirection(player, DirectionModifierEnum.Positive);
            }
            else
            {
                ModifyPlayerDirection(player, DirectionModifierEnum.Negative);
            }
        }

        private void ModifyPlayerDirection(Player player, DirectionModifierEnum modifier)
        {
            switch (modifier)
            {
                case DirectionModifierEnum.Positive:
                    player.DirectionBase = (player.DirectionBase + Constants.baseAngleSpeed)%360;
                    break;

                case DirectionModifierEnum.Negative:
                    player.DirectionBase = (player.DirectionBase - Constants.baseAngleSpeed + 360)%360;
                    break;

                default:
                    throw new ArgumentException("Unknown modifier: " + modifier);
            }
        }

        private void ChangePlayersPosition(GameSession gameSession)
        {
            foreach (var player in gameSession.Players)
            {
                var newCoor = GetPlayerNewPosition(player);

<<<<<<< HEAD
                if (IsNewCoorValid(newCoor) && !player.IsParalized && (DateTime.Now - player.LastDirectionChangeEventDate).TotalMilliseconds < 2000)
=======
                if (!player.IsParalized && IsNewCoorValid(newCoor))
>>>>>>> origin/develop
                {
                    player.Position = newCoor;
                }
            }
        }

        private Vector2 GetPlayerNewPosition(Player player)
        {
            var directionInRadians = GetRadiansFromDegrees(player.DirectionBase);

            var angleSin = Math.Sin(directionInRadians);
            var angleCos = Math.Cos(directionInRadians);

            var delta = new Vector2((float)(Constants.baseLineSpeed * angleCos), (float)(Constants.baseLineSpeed * angleSin));

            return player.Position + delta;
        }

        private double GetRadiansFromDegrees(int angle) => angle * Math.PI / 180;

        private bool IsNewCoorValid(Vector2 vector)
        {
            return vector.X >= 0
                && vector.X <= Constants.ClientWindowWidth * Constants.WorldDimensionsMultiplier
                && vector.Y >= 0
                && vector.Y <= Constants.ClientWindowHeight * Constants.WorldDimensionsMultiplier;
        }
    }
}