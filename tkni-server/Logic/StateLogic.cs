﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

using tkni_common;
using tkni_common.Dto;

using tkni_server.Entities;
using tkni_server.Enum;

namespace tkni_server.Logic
{
    public class StateLogic
    {
        private GameSession _gameSession;

        private static readonly Random rnd = new Random();

        private StateLogic() { }

        public static StateLogic Single { get; } = new StateLogic();

        public void ProcessGameState()
        {
            if (_gameSession == null)
            {
                return;
            }

            ProcessAttacks();
            ProcessStamensState();
            ChangePlayersDirections(_gameSession);
            ChangePlayersPosition(_gameSession);
        }

        public void CreateGameSession(string playerID)
        {
            _gameSession = new GameSession();

            RegisterNewPlayerToGameSession(playerID);

            _gameSession.Players.AddRange(GetTestBots());
        }

        public void RegisterNewPlayerToGameSession(string playerID)
        {
            Player newPlayer = new Player
            {
                ID = playerID,
                Position = GetNewPlayerPosition(),
                Name = GetRandomPlayerName()
            };

            _gameSession.Players.Add(newPlayer);
        }

        private string GetRandomPlayerName()
        {
            var num = rnd.Next(Constants.PlayerNames.Count());

            return Constants.PlayerNames[num];
        }

        public bool IsGameSessionExists => _gameSession != null;

        public void ProcessStamensState()
        {
            foreach (Player player in _gameSession.Players.Where(p => p.StamenDuration > Constants.StamenInertionDuration))
            {
                player.StamenDirection = null;
            }
        }

        public void ProcessAttacks()
        {
            List<Player> attackingPlayers = _gameSession.Players
                .Where(p => p.IsStamenActive && !p.IsDead)
                .ToList();

            var battlePairs = new List<(string who, string whom)>();
            Dictionary<string, int> playersAttackState = _gameSession.Players.ToDictionary(x => x.ID, y => 0);

            foreach (Player player in attackingPlayers.Where(p => playersAttackState[p.ID] == 0))
            {
                Vector2 stamenVector = player.StamenVector;

                IEnumerable<Player> victims = _gameSession.Players
                    .Except(Enumerable.Repeat(player, 1))
                    .Where(p => IsPlayerHitsOther(player.Position, stamenVector, p.Position));

                foreach (Player victim in victims)
                {
                    ReleaseVictimsOfVictim(victim.ID);

                    battlePairs.Add((player.ID, victim.ID));

                    playersAttackState[victim.ID]++;
                }
            }

            foreach (Player player in _gameSession.Players)
            {
                player.AttackersCount = playersAttackState[player.ID];

                if (!player.IsDead && player.AttackersCount >= Constants.AttacksToKill)
                {
                    player.IsDead = true;
                    player.IsDeadEventDate = DateTime.Now;
                }
            }

            void ReleaseVictimsOfVictim(string id)
            {
                var attackedByOtherPlayer = battlePairs.Where(x => x.who == id).ToArray();

                foreach (var entry in attackedByOtherPlayer)
                {
                    playersAttackState[entry.whom]--;
                }

                battlePairs.RemoveAll(x => x.who == id);
            }
        }

        public void AddAttackOnPlayer(string playerID)
        {
            Player currentPlayer = _gameSession.Players.FirstOrDefault(x => x.ID == playerID);

            if (currentPlayer != null)
            {
                currentPlayer.AttackersCount++;
            }
        }

        public IEnumerable<string> GetRegisteredPlayersIDs()
        {
            return _gameSession?.Players.Select(p => p.ID);
        }

        public void AssignPlayerDirectionChange(string playerID, int directionChange)
        {
            Player currentPlayer = _gameSession.Players.FirstOrDefault(x => x.ID == playerID);

            if (currentPlayer != null)
            {
                currentPlayer.DirectionChange = directionChange;
                currentPlayer.LastDirectionChangeEventDate = DateTime.Now;
                if (!currentPlayer.DirectionBase.HasValue)
                {
                    currentPlayer.DirectionBase = directionChange;
                }
            }
        }

        public void ActivatePlayerStamen(string playerID, int stamenDirection)
        {
            Player currentPlayer = _gameSession.Players.FirstOrDefault(x => x.ID == playerID);

            if (currentPlayer == null || currentPlayer.IsParalized)
            {
                return;
            }

            currentPlayer.StamenDirection = stamenDirection;
            currentPlayer.StamenDirectionChangeEventDate = DateTime.Now;
        }

        public SceneState GetPlayerSceneState(string playerID)
        {
            Player currentPlayer = _gameSession.Players.FirstOrDefault(x => x.ID == playerID);

            if (currentPlayer == null)
            {
                return null;
            }

            return new SceneState
            {
                BackgroundX = (int)(-currentPlayer.Position.X),
                BackgroundY = (int)(-currentPlayer.Position.Y),
                HitDirection = currentPlayer.StamenDirection,
                Others = GetVisiblePlayersInfo(currentPlayer).ToList(),
                IsMoving = currentPlayer.DirectionBase.HasValue,
                Name = currentPlayer.Name,
                IsKilled = currentPlayer.IsDead,
                AttackersCount = currentPlayer.AttackersCount
            };
        }

        public IEnumerable<PlayerInfo> GetVisiblePlayersInfo(Player currentPlayer)
        {
            IEnumerable<Player> visiblePlayers = _gameSession.Players
                .Where(p => p.ID != currentPlayer.ID && IsPlayerVisible(currentPlayer, p));

            List<PlayerInfo> visiblePlayersInfo = new List<PlayerInfo>();

            foreach (Player player in visiblePlayers)
            {
                Vector2 uIvector = CountUIVector(currentPlayer.Position, player.Position);

                visiblePlayersInfo.Add(new PlayerInfo
                {
                    ID = player.ID,
                    X = (int)uIvector.X,
                    Y = (int)uIvector.Y,
                    AttackersCount = player.AttackersCount,
                    StamenDirection = player.StamenDirection,
                    IsDead = player.IsDead,
                    Name = player.Name
                });
            }

            return visiblePlayersInfo;
        }

        private Vector2 CountUIVector(Vector2 currentUserVector, Vector2 otherUserVector)
        {
            Vector2 baseUIVector = Constants.ClientWindowSize / 2f;

            return otherUserVector - currentUserVector + baseUIVector;
        }

        public bool IsPlayerVisible(Player currentPlayer, Player otherPlayer)
        {
            if (otherPlayer.IsDead && otherPlayer.DeadDuration > Constants.PlayerDeadInertionDuration)
            {
                return false;
            }

            Vector2 distance = otherPlayer.Position - currentPlayer.Position;

            return Math.Abs(distance.X) < Constants.ClientWindowWidth / 2
                && Math.Abs(distance.Y) < Constants.ClientWindowHeight / 2;
        }

        private bool IsVectorIntersectsCircle(Vector2 p1, Vector2 p2, Vector2 center, float radius)
        {
            float x01 = p1.X - center.X;
            float y01 = p1.Y - center.Y;
            float x02 = p2.X - center.X;
            float y02 = p2.Y - center.Y;

            float dx = x02 - x01;
            float dy = y02 - y01;

            float a = dx * dx + dy * dy;
            float b = 2.0f * (x01 * dx + y01 * dy);
            float c = x01 * x01 + y01 * y01 - radius * radius;

            if (-b < 0) return (c < 0);
            if (-b < (2.0f * a)) return (4.0f * a * c - b * b < 0);
            return (a + b + c < 0);
        }

        private bool IsOnStamen(Vector2 vectorStart, Vector2 vectorEnd, Vector2 vectorIn)
        {
            Vector2 vectorStamen = vectorEnd - vectorStart;
            Vector2 vectorSum = vectorStamen + (vectorIn - vectorEnd);
            Vector2 vectorSub = vectorStamen - (vectorIn - vectorEnd);

            return
                vectorSum.Length() < vectorStamen.Length()
                && vectorSub.Length() > vectorStamen.Length();
        }

        private Vector2 GetNewPlayerPosition() => Constants.ClientWindowSize / 2;

        private void ChangePlayersDirections(GameSession gameSession)
        {
            foreach (Player player in gameSession.Players)
            {
                ChangePlayerDirection(player);
            }
        }

        private void ChangePlayerDirection(Player player)
        {
            if (!player.DirectionBase.HasValue || player.DirectionBase == player.DirectionChange)
            {
                return;
            }

            int directionDelta = (player.DirectionBase.Value - player.DirectionChange + 360) % 360;

            if (directionDelta > 180)
            {
                ModifyPlayerDirection(player, DirectionModifierEnum.Positive);
            }
            else
            {
                ModifyPlayerDirection(player, DirectionModifierEnum.Negative);
            }
        }

        private void ModifyPlayerDirection(Player player, DirectionModifierEnum modifier)
        {
            switch (modifier)
            {
                case DirectionModifierEnum.Positive:
                    player.DirectionBase = (player.DirectionBase + Constants.BaseAngleSpeed) % 360;
                    break;

                case DirectionModifierEnum.Negative:
                    player.DirectionBase = (player.DirectionBase - Constants.BaseAngleSpeed + 360) % 360;
                    break;

                default:
                    throw new ArgumentException("Unknown modifier: " + modifier);
            }
        }

        private void ChangePlayersPosition(GameSession gameSession)
        {
            foreach (var player in gameSession.Players.Where(p => !p.IsDead && p.DirectionBase.HasValue))
            {
                Vector2 newPosition = player.Position + Helpers.GetAngleVector(player.DirectionBase.Value) * (float)(Constants.BaseLineSpeed * Constants.ServerUpdateFrequency.TotalSeconds);

                if (!player.IsParalized && IsPositionValid(newPosition) && player.LastDirectionDuration < Constants.PlayerMoveInertionDuration)
                {
                    player.Position = newPosition;
                }
                else
                {
                    player.DirectionBase = null;
                }
            }
        }

        private bool IsPositionValid(Vector2 vector)
        {
            return vector.X >= 0
                && vector.X < Constants.WorldWidth
                && vector.Y >= 0
                && vector.Y < Constants.WorldHeight;
        }

        private IEnumerable<Player> GetTestBots()
        {
            yield return new Player
            {
                ID = "guidID1",
                Position = GetNewPlayerPosition() + new Vector2(50, 50),
                DirectionBase = 180,
                DirectionChange = 180,
                Name = "Bot"
            };

            yield return new Player
            {
                ID = "guidID2",
                Position = GetNewPlayerPosition() + new Vector2(150, 150),
                DirectionBase = 180,
                DirectionChange = 180,
                Name = "Bot"
            };

            yield return new Player
            {
                ID = "guidID3",
                Position = GetNewPlayerPosition() + new Vector2(300, 300),
                DirectionBase = 180,
                DirectionChange = 180,
                Name = "Bot"
            };

            yield return new Player
            {
                ID = "guidID4",
                Position = GetNewPlayerPosition() + new Vector2(-150, -150),
                DirectionBase = 180,
                DirectionChange = 180,
                Name = "Bot"
            };
        }

        private bool IsPlayerHitsOther(Vector2 firstPlayerPosition, Vector2 stamenVector, Vector2 secondPlayerPosition)
        {
            return IsVectorIntersectsCircle(firstPlayerPosition, stamenVector, secondPlayerPosition, Constants.PlayerBodyRadius);
        }
    }
}